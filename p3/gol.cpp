/*
 * CSc103 Project 3: Game of Life
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
 *instruction from same section classmates, Bashir
 *
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours:
 */
#include <iostream>
using namespace std;

#include <cstdio>
#include <stdlib.h> // for exit();
#include <getopt.h> // to parse long arguments.
#include <unistd.h> // sleep
#include <vector>
using std::vector;
#include <string>
using std::string;


static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Text-based version of Conway's game of life.\n\n"
"   --seed,-s     FILE     read start state from FILE.\n"
"   --world,-w    FILE     store current world in FILE.\n"
"   --fast-fw,-f  NUM      evolve system for NUM generations and quit.\n"
"   --help,-h              show this message and exit.\n";

size_t max_gen = 0;/* if > 0, fast forward to this generation. */
string wfilename =  "/tmp/gol-world-current"; /* write state here */
FILE* fworld = 0; /* handle to file wfilename. */
string initfilename = "/tmp/gol-world-current"; /* read initial state from here. */

vector<vector<bool>  > g;
size_t isize, jsize;



size_t nbrCount(size_t i, size_t j, const vector<vector<bool> >& g)
{

	size_t n = 0;
	size_t l = g.size();
	size_t w = g[0].size();

	bool left= 				g[i][(j-1+w)%w];
										if (left == true) ++n;

	bool right=       g[i][(j+1)%w];
										if (right == true) ++n;

	bool upper=       g[(i-1+w)%l][j];
										if (upper == true) ++n;

	bool upperier=    g[(i-1+l)%l][(j-1+w)%w];
										if (upperier == true) ++n;

	bool lowerier= 		g[(i+1)%l][(j-1)%w];
										if (lowerier == true) ++n;

	bool uppest=			g[(i-1+l)%l][(j+1)%w];
										if (uppest == true) ++n;
	bool lowest=      g[(i+1)%l][(j+1)%w];
										if (lowest == true) ++n;

	return n;


}





void update(){

	for(size_t i=0;i<g[0].size();++i)
	{
			for(size_t j=0; i<g[0].size();++j)
				{
							if(nbrCount(i,j,g)<2) g[i][j]=false;
							else if(nbrCount(i,j,g)>3) g[i][j]=false;
							else if(nbrCount(i,j,g)==3) g[i][j]=true;}}

			for(size_t i=0 ; i<g.size(); ++i)
							{
									for(size_t j=0; i<g[0].size(); ++j)
									{ if(g[i][j] == true) cout<<"0";
												else if (g[i][j] == false) cout<<".";}}
											}




int initFromFile(const string& fname)
		{
			char c;
			for(size_t i=0; i<g.size(); i++)
			{
						for(size_t j=0;i<g[0].size();++j)

			{fread(&c,1,1,fworld);
						g[i][j]=c;}}

      fclose(fworld);
			jsize = g.size();

			return 1;
		}


void mainLoop(){
		int s;
		vector<vector<bool>> v(jsize,vector<bool>(isize));

		for (int j=0; j< jsize; j++){
					for(int i=0; i<isize; i++) {
							s = nbrCount(i,j,g);

							if(g[i][j] && (s > 3 || s < 2))
										v[i][j] = false;

							else if(g[j][i] && (s == 2 || s==3))
										v[i][j] = true;

							else if (!g[j][i] && s == 3)
										v[j][i] = true;

						}

			}
		//sleep(0.2);
			g=v;

		}






void dumpState(FILE* f)
	{
	const char* c = ".0/n";

	for (size_t j = 0;j <jsize;j++){
		for (size_t i = 0; i < isize; ++i){

			if ( g[i][j])

			fwrite(&c,1,1,f);

		  else
			fwrite(&c,1,1,f);
			}
		fwrite(&c,1,1,f);
	}
}



char text[3] = ".O";

int main(int argc, char *argv[]) {
	// define long options
	static struct option long_opts[] = {
		{"seed",    required_argument, 0, 's'},
		{"world",   required_argument, 0, 'w'},
		{"fast-fw", required_argument, 0, 'f'},
		{"help",    no_argument,       0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "hs:w:f:", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case 's':
				initfilename = optarg;
				break;
			case 'w':
				wfilename = optarg;
				break;
			case 'f':
				max_gen = atoi(optarg);
				break;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}

	dumpState(fworld);
	fclose(fworld);
	update();
	return 0;
}






